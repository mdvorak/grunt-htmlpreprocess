var path = require('path');
var fs = require('fs');

module.exports = {
    'static-include': {
        template: '<div></div>',
        inheritAttributes: true,
        link: function (scope, element, attrs) {
            var src = attrs['src'];

            if (src[0] === "/") {
                src = path.join(scope.$cwd, src);
            } else {
                src = path.join(scope.$dir, src);
            }

            element.removeAttr('src');
            element.html(fs.readFileSync(src));
        }
    }
};
