/*
 * grunt-htmlpreprocess
 * https://bitbucket.org/mdvorak/grunt-htmlpreprocess
 *
 * Copyright (c) 2014 Michal Dvorak
 * Licensed under the MIT license.
 */

"use strict";

module.exports = function (grunt) {
    var cheerio = require('cheerio');
    var path = require('path');
    var builtinDir = path.join(__dirname, '../directives');

    grunt.registerMultiTask('htmlPreprocess', function () {
        var done = this.async();

        var files = this.files.concat(),
            options = this.options(),
            data = options.data || {},
            builtin = options.builtin || false,
            directiveList = grunt.file.expand(options.directives || []),
            directives = [];

        // Merge target
        directives.push({});

        // Builtin
        if (typeof builtin === "object") {
            // Select
            Object.keys(builtin).forEach(function (key) {
                if (builtin[key]) {
                    // Add to the list
                    directiveList.unshift(path.join(builtinDir, key + '.js'));
                }
            });
        } else if (builtin) {
            // All
            grunt.file.expand(path.join(builtinDir, '*.js')).forEach(function (file) {
                directiveList.unshift(file);
            });
        }

        // Debug
        grunt.log.ok("Using directives from files:");
        grunt.log.ok(directiveList.join('\n') + "\n");

        // Load directives
        directiveList.forEach(function (dir) {
            directives.push(require(dir));
        });

        // Merge directives into one object
        directives = extend.apply(null, directives);

        // Main parse function
        function parse(block, cwd, file) {
            // Parse
            var container = cheerio.load(block).root();

            // Evaluate
            function createScope(element, scopeDef) {
                var scope = {$data: data, $cwd: cwd, $file: file, $dir: path.dirname(file)};

                if (scopeDef) {
                    Object.keys(scopeDef).forEach(function (attr) {
                        scope[attr] = element.attr(attr) || element.attr('data-' + attr) || scopeDef[attr];
                    });
                }

                return scope;
            }

            function createAttrs(element) {
                var attrs = {},
                    attribs = element.get(0).attribs;

                for (var attr in attribs) {
                    if (attribs.hasOwnProperty(attr)) {
                        attrs[attr] = element.attr(attr);
                    }
                }

                return attrs;
            }

            function traverseNode(node) {
                var children = node.children();

                for (var i = 0; i < children.length; i++) {
                    var childNode = children.get(i),
                        child = children.eq(i);

                    if (directives.hasOwnProperty(childNode.name)) {
                        var directive = directives[childNode.name],
                            template = directive.template,
                            scope = createScope(child, directive.scope),
                            attrs = createAttrs(child),
                            html;

                        // Process
                        if (template) {
                            for (var expression in scope) {
                                if (scope.hasOwnProperty(expression)) {
                                    var value = scope[expression];
                                    template = template.replace(new RegExp('{{' + expression + '}}', 'g'), value);
                                }
                            }

                            html = child.html();
                            child = cheerio(template);

                            // Merge attributes
                            if (directive.inheritAttributes) {
                                for (var attr in attrs) {
                                    if (attrs.hasOwnProperty(attr)) {
                                        if (attr === "class" || attr === "data-class") {
                                            child.addClass(attrs[attr]);
                                        } else {
                                            child.attr(attr, attrs[attr]);
                                        }
                                    }
                                }
                            }

                            // Transclude
                            if (directive.transclude) {
                                child.find('transclude').replaceWith(html);
                            }
                        }

                        if (typeof directive.link === "function") {
                            directive.link(scope, child, attrs, html);
                        }

                        if (template) {
                            children.eq(i).replaceWith(child);
                        }
                    }

                    // Recursion
                    traverseNode(child);
                }
            }

            traverseNode(container);

            // Return modified html
            return container.html();
        }

        function process() {
            if (files.length <= 0) {
                done();
                return;
            }

            var file = files.pop();

            grunt.log.ok("Processing file " + file.src[0]);

            var content = grunt.file.read(file.src[0], {encoding: 'UTF-8'});
            var converted = parse(content, file.orig.cwd, file.src[0]);

            grunt.file.write(file.dest, converted);

            process();
        }

        process();
    });

    function extend(target) {
        var i, obj;

        function copyValue(key) {
            target[key] = obj[key];
        }

        for (i = 1; i < arguments.length; i++) {
            obj = arguments[i];
            Object.keys(obj).forEach(copyValue);
        }

        return target;
    }
};
